<?php
include("file_php/connect.php");
include("file_php/userQuery.php");
connect();
$row_ut=userQuery();
?>
<!DOCTYPE html>
<!--
To change this license header, choose License Headers in Project Properties.
To change this template file, choose Tools | Templates
and open the template in the editor.
-->
<html>
    <head>
        <meta charset="UTF-8">
        <title>PWS - Private Weather Station</title>
        <link rel="stylesheet" href="style/project.css" type="text/css">
    </head>
    <body>
        <h1 class="fontface">Benvenuto <?php echo $row_ut[0];?>!</h1>
      <div id="container">        
			

			<?php
				$file = fopen($row_ut[1], "r") or exit("Unable to open file!");
				//http://quartuweather.altervista.org/
				//Output a line of the file until the end is reached 
				while(!feof($file))
				{
					
					$plik = fgets($file);
					
				}
				fclose($file);
				$array = explode(" ",$plik);

                                echo '<table>';
                                echo '<tr align="top">';
                                echo '<td width="70%">';
				echo '<div id="box" class="altriDati" width="50%">';
                                
                                echo '<div id="temp">Temperatura '.$array[4].'</div>'; 
                                echo "<div id='um'>Umidit&agrave ".$array[5]."%"."</div>";
                                echo "<div id='bar'>Barometro ".$array[6]."hPa</div>";
				$vento = $array[1] * 1.852;
                                echo "<div id='wind'>Vento ".$array[1]."K  =".$vento."km/h</div>";
				$windDir = $array[3];
				if($windDir == 0){$windDir = "N";}
				if($windDir > 0 && $windDir < 45){$windDir = "NNE"; }
                                if($windDir == 45){$windDir = "NE"; }
                                if($windDir > 45 && $windDir < 90){$windDir = "NEN"; }
                                if($windDir == 90){$windDir = "E"; }
                                if($windDir > 90 && $windDir < 135){$windDir = "ESE"; }
                                if($windDir == 135){$windDir = "SE"; }
                                if($windDir > 135 && $windDir < 180){$windDir = "SSE"; }
                                if($windDir == 180){$windDir = "S"; }
                                if($windDir > 180 && $windDir < 225){$windDir = "SSW"; }
                                if($windDir == 225){$windDir = "SW"; }
                                if($windDir > 225 && $windDir < 270){$windDir = "WSW"; }
                                if($windDir == 270){$windDir = "W"; }
                                if($windDir > 270 && $windDir < 315){$windDir = "NWN"; }
                                if($windDir == 315){$windDir = "NW"; }
                                if($windDir > 315 && $windDir < 360){$windDir = "NNW"; }
                                if($vento == 0){$windDir = "calm";}
                                echo "<div id='windir'>Direzione del vento <img class='vento' name='windDir' src='images/".$windDir.".png'></div>";
                               
                                echo "</div>";//altriDati
                                echo '</td>';
                                echo '<td width="30%">';
                                echo '<div id="data" class="data"  align="right"><p>Oggi &egrave '.$array[74]."</p></div>";
                                echo '</td>';
                                echo '</tr>';
                                echo '<table>';
                       
			
		
        
                echo '<div id="foot" align="bottom">';
                echo 'Created by odkrywczyni. All Rights Reserved.';
                echo '</div>';
              ?>

    </body>
</html>
